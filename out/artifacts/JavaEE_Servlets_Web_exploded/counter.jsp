<%--
  Created by IntelliJ IDEA.
  User: Alisher
  Date: 9/15/2022
  Time: 9:27 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<h1>
    <%
        HttpSession session1 = request.getSession();
        Integer sessionValue =(Integer) session1.getAttribute("counterValue");

    %>
</h1>
<h2>
    <a href="/counter?increement=0">
        -
    </a>
</h2>
<%
    int counterValue = 0;
    Cookie[] cookies = request.getCookies();
    if (cookies != null) {
        for (Cookie cookie : cookies) {
            if (cookie.getName().equals("counterValue")) {
                counterValue = Integer.parseInt(cookie.getValue());
            }
        }
    }
    out.println("<input type=\"text\" value=" + sessionValue + ">");

%>
<h2>
    <a href="/counter?increement=1">
        +
    </a>
</h2>

</body>
</html>
