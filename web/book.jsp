<%--
  Created by IntelliJ IDEA.
  User: Alisher
  Date: 9/13/2022
  Time: 11:10 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Title</title>
</head>
<body>
<h1>This is admin Cabinet</h1>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.List" %>
<%@ page import="uz.pdp.model.Book" %>
<%
    List<Book> books = (List<Book>) request.getAttribute("books");
%>

<button><a href="/saveOrEdit">Add NEW BOOK</a></button>
<table style="border: black solid 1px">
    <tr style="border: black solid 1px">
        <th style="border: black solid 1px">id</th>
        <th style="border: black solid 1px">name</th>
        <th style="border: black solid 1px">action</th>
    </tr>
    <%
        for (Book book : books) {
            out.println("<tr style=\"border: black solid 1px\">");
            out.println("<td style=\"border: black solid 1px\">"+book.getId());
            out.println("</td>");
            out.println("<td style=\"border: black solid 1px\">"+book.getName());
            out.println("</td>");
            out.println("<td style=\"border: black solid 1px\">");
            out.println("<button><a href=\"/saveOrEdit?id="+book.getId()+"\">EDIT</a></button>");
            out.println("<button><a href=\"/delete?id="+book.getId()+"\">DELETE</a></button>");
            out.println("</td>");
            out.println("</tr>");
        }
    %>
</table>
</body>
</html>
