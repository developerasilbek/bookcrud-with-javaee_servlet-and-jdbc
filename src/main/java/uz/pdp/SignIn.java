package uz.pdp;

import lombok.SneakyThrows;
import uz.pdp.model.User;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class SignIn extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.sendRedirect("/signIn.jsp");
    }

    @SneakyThrows
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        User user = DBConnection.getUserByUsernameAndPassword(username, password);
        if (user == null) {
            response.sendRedirect("/signIn");
        } else {
            HttpSession session = request.getSession();
            session.setAttribute("user",user);
            if (user.getRoleName().equals("ADMIN"))
                response.sendRedirect("/adminCabinet");
            else
                response.sendRedirect("/userCabinet.jsp");
        }
    }
}
