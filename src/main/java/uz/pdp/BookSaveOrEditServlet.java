package uz.pdp;

import lombok.SneakyThrows;
import uz.pdp.model.Book;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.IOException;

public class BookSaveOrEditServlet extends HttpServlet {
    @SneakyThrows
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String id = request.getParameter("id");
        if (id!=null){
            Book bookById = DBConnection.getBookById(Integer.parseInt(id));
            request.setAttribute("book",bookById);
        }
        RequestDispatcher dispatcher = request.getRequestDispatcher("/savebook.jsp");
        dispatcher.forward(request,response);
    }

    @SneakyThrows
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String id = request.getParameter("id");
        String name = request.getParameter("name");
        DBConnection.saveOrEdit(id,name);
        response.sendRedirect("/book");
    }
}
