package uz.pdp.util;

import uz.pdp.DBConnection;
import uz.pdp.model.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class CheckSession {
    public static Boolean checkSession(HttpServletRequest request,String roleName){
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        if (user == null){
            return null;
        }else {
           if (user.getRoleName().equals(roleName)){
               return true;
           }else {
               return false;
           }
        }
    }
}
