package uz.pdp;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;

public class CounterServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String increement = request.getParameter("increement");
        int value = 0;
        Cookie[] cookies = request.getCookies();
        if (cookies !=null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("counterValue")) {
                    value = Integer.parseInt(cookie.getValue());
                }
            }
        }
        HttpSession session = request.getSession();
        Integer attribute =(Integer) session.getAttribute("counterValue");

        if (increement == null) {
            attribute=0;
            Cookie cookie = new Cookie("counterValue", String.valueOf(value));
            cookie.setMaxAge(500);
            response.addCookie(cookie);
        } else {
            if (increement.equals("1")) {
                attribute++;
                Cookie cookie = new Cookie("counterValue", String.valueOf((++value)));
                cookie.setMaxAge(500);
                response.addCookie(cookie);
            } else {
                attribute--;
                Cookie cookie = new Cookie("counterValue", String.valueOf((--value)));
                cookie.setMaxAge(500);
                response.addCookie(cookie);
            }
        }
        response.addCookie(new Cookie("Tesha1","Boltaev11"));
        response.addCookie(new Cookie("Tesha2","Boltaev21"));
        response.addCookie(new Cookie("Tesha1","Boltaev12"));
        response.addCookie(new Cookie("Tesha2","Boltaev22"));

        session.setAttribute("counterValue",attribute);
        response.sendRedirect("/counter.jsp");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
