package uz.pdp;

import uz.pdp.model.Book;
import uz.pdp.util.CheckSession;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

public class UserCabinet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Boolean user = CheckSession.checkSession(request, "USER");
        if (user != null && user) {
            response.sendRedirect("userCabinet.jsp");
        }else {
            HttpSession session = request.getSession();
            session.setAttribute("user",null);
            response.sendRedirect("/signIn");

        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
