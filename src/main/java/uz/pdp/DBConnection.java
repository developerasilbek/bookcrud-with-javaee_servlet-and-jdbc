package uz.pdp;

import uz.pdp.model.Book;
import uz.pdp.model.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class DBConnection {
    private static final String DB_URL="jdbc:postgresql://localhost:5432/bookcrud";
    private static final String DB_USERNAME="postgres";
    private static final String DB_PASSWORD="root123";

    public static Connection createConnection() throws ClassNotFoundException, SQLException {
        Class.forName("org.postgresql.Driver");
        return DriverManager.getConnection(DB_URL,DB_USERNAME,DB_PASSWORD);
    }

    public static List<Book> getBookList() throws SQLException, ClassNotFoundException {
        Connection connection = createConnection();
        ResultSet resultSet = connection.createStatement().executeQuery("select * from book;");
        List<Book> bookList = new ArrayList<>();
        while (resultSet.next()){
            Book book = new Book();
            book.setId(resultSet.getInt(1));
            book.setName(resultSet.getString(2));
            bookList.add(book);
        }
        return bookList;
    }

    public static Book getBookById(int parseInt) throws SQLException, ClassNotFoundException {
        Connection connection = createConnection();
        ResultSet resultSet = connection.createStatement().executeQuery("select * from book;");
        Book book =null;
        while (resultSet.next()){
            book=new Book();
            book.setId(resultSet.getInt(1));
            book.setName(resultSet.getString(2));
        }
        return book;
    }

    public static void saveOrEdit(String id, String name) throws SQLException, ClassNotFoundException {
        String query="";
        if (id != null){
            query="update book set name='"+name+"' where id ="+Integer.parseInt(id)+";";
        }else {
            query="insert into book(name) values ('"+name+"');";
        }
        Connection connection = createConnection();
        Statement statement = connection.createStatement();
        statement.execute(query);
    }

    public static void remove(String id) throws SQLException, ClassNotFoundException {
        Connection connection = DBConnection.createConnection();
        Statement statement = connection.createStatement();
        statement.execute("delete from book where id="+id+";");
    }

    public static User getUserByUsernameAndPassword(String username, String password) throws SQLException, ClassNotFoundException {
        Connection connection = DBConnection.createConnection();
        PreparedStatement prepareStatement = connection.prepareStatement("select u.*,r.role_name from users u join roles r on r.id = u.role_id where username=? and password=?;");
        prepareStatement.setString(1,username);
        prepareStatement.setString(2,password);
        ResultSet resultSet = prepareStatement.executeQuery();
        while (resultSet.next()){
            return new User(
              resultSet.getInt(1),
              resultSet.getString(2),
              resultSet.getString(3),
              resultSet.getString(4),
              resultSet.getString(6)
            );
        }
        return null;
    }
}
