package uz.pdp;

import lombok.SneakyThrows;
import uz.pdp.model.Book;
import uz.pdp.util.CheckSession;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class BookServlet extends HttpServlet {
    @SneakyThrows
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Boolean admin = CheckSession.checkSession(request, "ADMIN");
        if (admin != null && admin) {
            List<Book> bookList = DBConnection.getBookList();
            request.setAttribute("books", bookList);
            RequestDispatcher dispatcher = request.getRequestDispatcher("/book.jsp");
            dispatcher.forward(request, response);
        }else {
            HttpSession session = request.getSession();
            session.setAttribute("user",null);
            response.sendRedirect("/signIn");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
