package uz.pdp.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {
    private Integer id;
    private String fio;
    private String username;
    private String password;
    private String roleName;

    public User(String fio, String username, String password, String roleName) {
        this.fio = fio;
        this.username = username;
        this.password = password;
        this.roleName = roleName;
    }
}
