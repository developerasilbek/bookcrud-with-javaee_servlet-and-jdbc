package uz.pdp;

import lombok.SneakyThrows;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.IOException;

public class RemoveBook extends HttpServlet {
    @SneakyThrows
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String id = request.getParameter("id");
        DBConnection.remove(id);
        response.sendRedirect("/book");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
